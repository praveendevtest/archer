import collections
import fnmatch
import jinja2
from dataclasses import dataclass, field
from typing import Set, Dict, List, Optional


IGNORE_KEYS = {"__id__", "__sls__", "fun", "state", "order"}


def build(hub):
    """
    Take the compiled data and build the model
    """
    hub.archer.modeler.counts()
    hub.archer.modeler.macros()


def check_skip(hub, pathref):
    for pattern in hub.OPT.archer.ignore:
        if fnmatch.fnmatch(pathref, pattern):
            return True
    return False


def get_pathref(chunk) -> str:
    return f"{chunk['state']}.{chunk['fun']}"


def counts(hub):
    """
    Populate the counts
    """
    hub.archer.COUNTS = {}
    hub.archer.COMMON = {}
    for chunk in hub.archer.COMPILED["low"]:
        pathref = get_pathref(chunk)
        if hub.archer.modeler.check_skip(pathref):
            continue
        if pathref not in hub.archer.COUNTS:
            hub.archer.COUNTS[pathref] = {}
        # Populate counts
        for key, val in chunk.items():
            if key in IGNORE_KEYS:
                continue
            if not isinstance(val, collections.Hashable):
                val = _json_dump(val)
            if key not in hub.archer.COMMON:
                hub.archer.COMMON[key] = {"instances": 1, "vals": [val], "pathrefs": set([pathref])}
            else:
                hub.archer.COMMON[key]["instances"] += 1
                hub.archer.COMMON[key]["vals"].append(val)
                hub.archer.COMMON[key]["pathrefs"].add(pathref)
            if key not in hub.archer.COUNTS[pathref]:
                hub.archer.COUNTS[pathref][key] = {}
            if val not in hub.archer.COUNTS[pathref][key]:
                hub.archer.COUNTS[pathref][key][val] = 1
                continue
            else:
                hub.archer.COUNTS[pathref][key][val] += 1


def macros(hub):
    """
    Populate the raw macros data
    """
    hub.archer.MACROS = {}
    hub.archer.VARS = {}
    for pathref, vals in hub.archer.COUNTS.items():
        if pathref not in hub.archer.MACROS:
            hub.archer.MACROS[pathref] = {}
            hub.archer.VARS[pathref] = set()
        for key, count in vals.items():
            for val, num in count.items():
                if num == 1:
                    # Always the same, no variable needed
                    hub.archer.MACROS[pathref][key] = val
                else:
                    hub.archer.MACROS[pathref][key] = "__variable__"
                    hub.archer.VARS[pathref].add(key)


@dataclass
class StateInfo:
    # state.fun -> name -> **kwargs
    calls: Dict[str, Dict[str, object]] = field(default_factory=dict)
    # arg_popularity -> parameter_str -> [used_count, arg_data]
    arg_popularity: Dict[str, Dict[str, List]] = field(default_factory=dict)
    # arg -> parameter
    default_arg: Dict[str, object] = field(default_factory=dict)
    # arg
    static_arg: Set[str] = field(default_factory=set)


def object_key(obj: object) -> str:
    return f"{type(obj).__name__}-{obj}"


def optimizer_data(hub) -> Dict[str, StateInfo]:
    opt_data = {}
    # PASS 1
    # - build calls
    # - build arg_popularity
    for chunk in hub.archer.COMPILED["low"]:
        pathref = get_pathref(chunk)
        if hub.archer.modeler.check_skip(pathref):
            continue
        if pathref not in opt_data:
            opt_data[pathref] = StateInfo()
        info = opt_data[pathref]
        name = chunk["name"]
        info.calls[name] = {}
        for key, val in chunk.items():
            if key in IGNORE_KEYS or key == "name":
                continue
            info.calls[name][key] = val
            val_str = object_key(val)
            if key not in info.arg_popularity:
                info.arg_popularity[key] = {}
            if val_str not in info.arg_popularity[key]:
                info.arg_popularity[key][val_str] = [0, val]
            info.arg_popularity[key][val_str][0] += 1
    # PASS 2
    # - Find default_arg
    # - Find static_arg
    for _, info in opt_data.items():
        call_count = len(info.calls)
        for arg, arg_datas in info.arg_popularity.items():
            obj_count = 0
            used_count = 0
            obj = None
            for arg_data_str, val in arg_datas.items():
                used_count += val[0]
                if val[0] > obj_count:
                    obj_count, obj = val
            if used_count == call_count:
                # to be a default arg must be used at lest 10% of the time.
                if obj_count > call_count * 0.1:
                    info.default_arg[arg] = obj
                    if obj_count == call_count:
                        info.static_arg.add(arg)
    return opt_data


def _optimizer(fun_name: str,
               info: StateInfo) -> str:
    name_to_args = {}
    for call, args in info.calls.items():
        call_args = {}
        for arg, val in args.items():
            if arg in info.static_arg:
                continue
            if arg in info.default_arg:
                continue
            call_args[arg] = val
        name_to_args[call] = call_args

    name = fun_name.replace(".", "_")
    default_dict_name = f"{name}_defaults"

    defaults = info.default_arg.copy()
    for static_key in info.static_arg:
        defaults[static_key] = info.calls[list(info.calls.keys())[0]][static_key]
    name_to_args_json = _json_dump(name_to_args)
    defaults_json = _json_dump(defaults)

    jinja_yaml = f"{{% set {name} = {name_to_args_json} %}}\n\n"
    jinja_yaml += f"{{% for i in {name} %}}\n"
    jinja_yaml += f"{{% set {default_dict_name} = {defaults_json} %}}\n"
    jinja_yaml += f"{{% set _ = {default_dict_name}.update({name}[i]) %}}\n"
    jinja_yaml += "{%set kwargs = [] %}\n"
    jinja_yaml += f"{{% for k in {default_dict_name} %}}\n"
    jinja_yaml += f"{{% set _ = kwargs.append({{k: {default_dict_name}[k]}}) %}}\n"
    jinja_yaml += "{% endfor %}\n"
    jinja_yaml += f"{{{{ i + \":\"}}}}\n"
    jinja_yaml += f"  {fun_name + ':'}\n"
    jinja_yaml += "    {{ kwargs }}\n"
    jinja_yaml += f"{{% endfor %}}\n"
    return jinja_yaml


def optimizer(hub, opt_data: Dict[str, StateInfo], one_file: Optional[str] = None) -> Dict[str, str]:
    call_groups = {}
    for fun_name, info in opt_data.items():
        call_groups[fun_name.replace(".", "_")] = _optimizer(fun_name, info)

    if one_file:
        call_groups = {one_file: "\n".join([sls for _, sls in call_groups.items()])}
    return call_groups


# TODO
def trust(hub, compiled, call_groups: Dict[str, str]):
    pass


def _render_jinja(data: str):
    return jinja2.Template(data).render()


def _json_dump(data: object) -> str:
    return _json_dump_helper(data, 0)


def _json_dump_helper(data: object, level: int) -> str:
    if isinstance(data, (int, float)):
        return str(data)
    elif isinstance(data, str):
        if "\"" in data:
            data_tmp = ""
            for index, character in enumerate(data):
                if character == "\"":
                    slash_count = 0
                    at = index - 1
                    while at >= 0 and data[at] == "\\":
                        slash_count += 1
                        at += -1
                    if slash_count % 2 == 0:
                        data_tmp += "\\"
                data_tmp += character
            data = data_tmp
        return f'"{data}"'
    elif isinstance(data, bool):
        if data:
            return "true"
        return "false"
    elif data is None:
        return "null"

    indent = " " * level
    if isinstance(data, list):
        j = "["
        if data:
            if len(data) == 1:
                j += f"{_json_dump_helper(data[0], level + 1)}"
            else:
                j += f"{_json_dump_helper(data[0], level + 1)},\n"
            for item in data[1:-1]:
                j += f"{indent} {_json_dump_helper(item, level + 1)},\n"
            if len(data) >= 2:
                j += f"{indent} {_json_dump_helper(data[-1], level + 1)}"
        j += "]"
        return j
    elif isinstance(data, dict):
        j = "{"
        keys = list(data)
        if data:
            if len(keys) == 1:
                j += f"{_json_dump_helper(keys[0], level + 1)}: {_json_dump_helper(data[keys[0]], level + 1)}"
            else:
                j += f"{_json_dump_helper(keys[0], level + 1)}: {_json_dump_helper(data[keys[0]], level + 1)},\n"
            for item in keys[1:-1]:
                j += f"{indent} {_json_dump_helper(item, level + 1)}: {_json_dump_helper(data[item], level + 1)},\n"
            if len(keys) >= 2:
                j += f"{indent} {_json_dump_helper(keys[-1], level + 1)}: {_json_dump_helper(data[keys[-1]], level + 1)}"
        j += "}"
        return j
    raise NotImplementedError()
