# Import python libs
import pprint
import os
import jinja2


def __init__(hub):
    # Remember not to start your app in the __init__ function
    # This function should just be used to set up the plugin subsystem
    # The run.py is where your app should usually start
    for dyne in ["idem"]:
        hub.pop.sub.add(dyne_name=dyne)


def cli(hub):
    hub.pop.config.load(["archer"], cli="archer")
    # Your app's options can now be found under hub.OPT.archer
    kwargs = dict(hub.OPT.archer)

    # Initialize the asyncio event loop
    hub.pop.loop.create()

    # Start the async code
    coroutine = hub.archer.init.run(**kwargs)
    hub.pop.Loop.run_until_complete(coroutine)


async def run(hub, **kwargs):
    """
    This is the entrypoint for the async code in your project
    """
    hub.archer.COMPILED = await hub.archer.compiler.compile()
    # get visual data and write it to console
    if hub.OPT.archer.visualize:
        hub.archer.modeler.build()
        print("VARS")
        print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        pprint.pprint(hub.archer.VARS)
        print("MACROS")
        print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        pprint.pprint(hub.archer.MACROS)
        print("COUNTS")
        print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        pprint.pprint(hub.archer.COUNTS)
        print("COMMON")
        print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
        pprint.pprint(hub.archer.COMMON)
        print("XXX")

    # optimize data into jinja and yaml
    opt_data = hub.archer.modeler.optimizer_data()
    call_groups = hub.archer.modeler.optimizer(opt_data, hub.OPT.archer.one_file)
    hub.archer.OPTIMIZER_DATA = opt_data
    hub.archer.CALL_GROUPS = call_groups

    # write formatted sls to console
    if hub.OPT.archer.out_sls:
        for _, sls in call_groups.items():
            print(sls)

    out_dir = hub.OPT.archer.out_dir
    if hub.OPT.archer.one_file:
        if out_dir is None:
            out_dir = "."
    # write formatted sls to a file
    if out_dir:
        for name, sls in call_groups.items():
            with open(os.path.join(out_dir, f"{name}.sls"), "w") as file:
                file.write(sls)
            if hub.OPT.archer.render:
                with open(os.path.join(out_dir, f"{name}_render.sls"), "w") as file:
                    file.write(jinja2.Template(sls).render())
